package studentmanagement;

public class MarksNotInRange extends Exception{
    public MarksNotInRange() {
    }

    public MarksNotInRange(String message) {
        super(message);
    }

    public MarksNotInRange(String message, Throwable cause) {
        super(message, cause);
    }

    public MarksNotInRange(Throwable cause) {
        super(cause);
    }
}
