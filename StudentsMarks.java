package studentmanagement;

public class StudentsMarks  {
    private  int std_id;
    private  int std_rollnumber;
    private int phy;
    private int math;
    private int chem;
    private int eng;
    private int hin;



    public StudentsMarks() {
    }

    public StudentsMarks(int std_id, int std_rollnumber, int phy, int math, int chem, int eng, int hin) {
        this.std_id = std_id;
        this.std_rollnumber = std_rollnumber;
        this.phy = phy;
        this.math = math;
        this.chem = chem;
        this.eng = eng;
        this.hin = hin;
    }

    public int getStd_rollnumber() {
        return std_rollnumber;
    }

    public void setStd_rollnumber(int std_rollnumber) {
        this.std_rollnumber = std_rollnumber;
    }

    public int getStd_id() {
        return std_id;
    }

    public void setStd_id(int std_id) {
        this.std_id = std_id;
    }

    public int getPhy() {
        return phy;
    }

    public void setPhy(int phy) {
        this.phy = phy;
    }

    public int getMath() {
        return math;
    }

    public void setMath(int math) {
        this.math = math;
    }

    public int getChem() {
        return chem;
    }

    public void setChem(int chem) {
        this.chem = chem;
    }

    public int getEng() {
        return eng;
    }

    public void setEng(int eng) {
        this.eng = eng;
    }

    public int getHin() {
        return hin;
    }

    public void setHin(int hin) {
        this.hin = hin;
    }

    @Override
    public String toString() {
        return "StudentsMarks{" +
                "std_id=" + std_id +
                ", std_rollnumber=" + std_rollnumber +
                ", phy=" + phy +
                ", math=" + math +
                ", chem=" + chem +
                ", eng=" + eng +
                ", hin=" + hin +
                '}';
    }
}
