package studentmanagement;

import java.util.List;

public class Student {
    private int std_rollnumber;
    private int std_age;
    private String std_name;
    private  String std_address;
    private  String std_divison;
    private StudentsMarks marksList;
//    construtor

    public Student() {
    }




    public Student(int std_rollnumber, int std_age, String std_name, String std_address, String std_divison) {
        this.std_rollnumber = std_rollnumber;
        this.std_age = std_age;
        this.std_name = std_name;
        this.std_address = std_address;
        this.std_divison = std_divison;
    }

    public Student(int std_rollnumber, int std_age, String std_name, String std_address, String std_divison, StudentsMarks marksList) {
        this.std_rollnumber = std_rollnumber;
        this.std_age = std_age;
        this.std_name = std_name;
        this.std_address = std_address;
        this.std_divison = std_divison;
        this.marksList = marksList;
    }

    //    getter and setter


    public Student(StudentsMarks marksList) {
        this.marksList = marksList;
    }

    public StudentsMarks getMarksList() {
        return marksList;
    }

    public void setMarksList(StudentsMarks marksList) {
        this.marksList = marksList;
    }

    public int getStd_rollnumber() {
        return std_rollnumber;
    }

    public void setStd_rollnumber(int std_rollnumber) {
        this.std_rollnumber = std_rollnumber;
    }

    public int getStd_age() {
        return std_age;
    }

    public void setStd_age(int std_age) {
        this.std_age = std_age;
    }

    public String getStd_name() {
        return std_name;
    }

    public void setStd_name(String std_name) {
        this.std_name = std_name;
    }

    public String getStd_address() {
        return std_address;
    }

    public void setStd_address(String std_address) {
        this.std_address = std_address;
    }

    public String getStd_divison() {
        return std_divison;
    }

    public void setStd_divison(String std_divison) {
        this.std_divison = std_divison;
    }

//    to stirng


    @Override
    public String toString() {
        return "Student{" +
                "std_rollnumber=" + std_rollnumber +
                ", std_age=" + std_age +
                ", std_name='" + std_name + '\'' +
                ", std_address='" + std_address + '\'' +
                ", std_divison='" + std_divison + '\'' +
                ", marksList=" + marksList +
                '}';
    }
}
