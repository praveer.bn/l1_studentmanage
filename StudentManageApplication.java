package studentmanagement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StudentManageApplication {
    boolean flag=true;
    StudentManagementOperation studentManagementOperation=new StudentManagementOperation();
    BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
    public void studentMenu() throws IOException {
        while (flag){
        System.out.println("0 for exit");
        System.out.println("1 for add students");
        System.out.println("2 for display students details");
        System.out.println("3 for add students marks");
        System.out.println("4 for display student result ");
        System.out.println("enter your option");
        int option=Integer.parseInt(bufferedReader.readLine());
        switch (option){
            case 0:
                System.exit(0);
            case 1:
                studentManagementOperation.addStudentsBasicDetails();
                break;
            case 2:
                studentManagementOperation.showStudentsBasicDetails();
                break;
            case 3:
                studentManagementOperation.addMarks();
                break;
            case 4:
                studentManagementOperation.showMarks();
                break;
            default:
                System.out.println("WRONG INPUT");
        }

    }}

    public static void main(String[] args) throws IOException {
        new StudentManageApplication().studentMenu();
    }
}
