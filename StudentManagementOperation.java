package studentmanagement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;
class SortByStudentName implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        return o1.getStd_name().compareTo(o2.getStd_name());
    }
}
public class StudentManagementOperation {
    ArrayList<Student> studentArrayList=new ArrayList<>();
    ArrayList<StudentsMarks> studentsMarksArrayList=new ArrayList<>();
    StudentsMarks studentsMarks;
    BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
    Student student;

    public void  addStudentsBasicDetails() throws IOException {
        System.out.println("enter the roll number");
        int studentRollNo = Integer.parseInt(bufferedReader.readLine());

        List<Student> students = studentArrayList.stream().filter(student1 -> student1.getStd_rollnumber() == studentRollNo).collect(Collectors.toList());
        if(students.size()>0) {
            try{
                throw new DuplicateStudentException("roll number already exists");
            }catch (DuplicateStudentException duplicateStudentException){
                duplicateStudentException.printStackTrace();
            }
        }
        else {
            student =new Student();
            student.setStd_rollnumber(studentRollNo);
            System.out.println("enter the name");
            student.setStd_name(bufferedReader.readLine());
            System.out.println("enter the division");
            student.setStd_divison(bufferedReader.readLine());
            System.out.println("enter the age");
            student.setStd_age(Integer.parseInt(bufferedReader.readLine()));
            System.out.println("enter the address");
            student.setStd_address(bufferedReader.readLine());

            studentArrayList.add(student);
        }
    }
    public void showStudentsBasicDetails(){
//        Collections.sort(studentArrayList,new SortByStudentName());
        List<Student> students=studentArrayList.stream().sorted(Comparator.comparing(s->s.getStd_rollnumber())).collect(Collectors.toList());
        students=students.stream().sorted(Comparator.comparing(s->s.getStd_name())).collect(Collectors.toList());
        //        studentArrayList.stream().map((s1,s2)->s1.getStd_name().equals(s2.getStd_name())))
        students.stream().forEach(System.out::println);
    }

    public void addMarks() throws IOException {
        System.out.println("enter the id");
        int id=Integer.parseInt(bufferedReader.readLine());
        List<StudentsMarks> students=studentsMarksArrayList.stream().filter(studentsMarks -> studentsMarks.getStd_id()==id).collect(Collectors.toList());
        if(students.size()>0){
try{
    throw new StudentNotFoundException("id already exists");
}catch (StudentNotFoundException studentNotFoundException){
    studentNotFoundException.printStackTrace();
}
        }else {
            studentsMarks = new StudentsMarks();
            studentsMarks.setStd_id(id);
            System.out.println("enter the roll number");
            studentsMarks.setStd_rollnumber(Integer.parseInt(bufferedReader.readLine()));
            Student student1 = studentArrayList.stream().filter(s -> s.getStd_rollnumber() == studentsMarks.getStd_rollnumber()).findFirst().get();
            System.out.println("enter the phy marks");
            int phy = Integer.parseInt(bufferedReader.readLine());
            if (phy > 100 && phy < 0) {
                try {
                    throw new MarksNotInRange("marks not in range");
                } catch (MarksNotInRange marksNotInRange) {
                    marksNotInRange.printStackTrace();
                }
            } else {
                if (phy < 35) {
                    studentsMarks.setPhy(phy + 5);
                } else {
                    studentsMarks.setPhy(phy);

                }
            }
            System.out.println("enter the math marks");
            int math = Integer.parseInt(bufferedReader.readLine());
            if (math > 100 && math < 0) {
                try {
                    throw new MarksNotInRange("marks not in range");
                } catch (MarksNotInRange marksNotInRange) {
                    marksNotInRange.printStackTrace();
                }
            } else {

                if (math < 35) {
                    studentsMarks.setMath(math + 5);
                } else {
                    studentsMarks.setMath(math);
                }
            }
            System.out.println("enter the chem marks");
            int chem = Integer.parseInt(bufferedReader.readLine());
            if (phy > 100 && phy < 0) {
                try {
                    throw new MarksNotInRange("marks not in range");
                } catch (MarksNotInRange marksNotInRange) {
                    marksNotInRange.printStackTrace();
                }
            } else {
                if (chem < 35) {
                    studentsMarks.setChem(chem + 5);
                } else {
                    studentsMarks.setChem(chem);

                }
            }
            System.out.println("enter the eng marks");
            int eng = Integer.parseInt(bufferedReader.readLine());
            if (phy > 100 && phy < 0) {
                try {
                    throw new MarksNotInRange("marks not in range");
                } catch (MarksNotInRange marksNotInRange) {
                    marksNotInRange.printStackTrace();
                }
            } else {
                if (eng < 35) {
                    studentsMarks.setEng(eng + 5);
                } else {
                    studentsMarks.setEng(eng);

                }
            }
            System.out.println("enter the hin marks");
            int hin = Integer.parseInt(bufferedReader.readLine());
            if (phy > 100 && phy < 0) {
                try {
                    throw new MarksNotInRange("marks not in range");
                } catch (MarksNotInRange marksNotInRange) {
                    marksNotInRange.printStackTrace();
                }
            } else {
                if (hin < 35) {
                    studentsMarks.setHin(hin + 5);
                } else {
                    studentsMarks.setHin(hin);
                }
                student1.setMarksList(studentsMarks);
            }
        }

    }
    public  void showMarks() throws IOException {
        System.out.println("enter the roll number");
        int rollnumber=Integer.parseInt(bufferedReader.readLine());
        studentArrayList.stream().filter(item->item.getStd_rollnumber()==rollnumber).forEach(System.out::println);
    }
}
